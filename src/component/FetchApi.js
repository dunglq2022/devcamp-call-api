import { Component } from "react";

class FetchApi extends Component {

    fetchApi = async (url, requestOptions) => {
        let response = await fetch(url, requestOptions);
        let data =  await response.json();

        return(data)
    }

    getById = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        var url = "https://jsonplaceholder.typicode.com/posts/1"
        this.fetchApi(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }
    getAllId = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        var url = "https://jsonplaceholder.typicode.com/posts/"
        this.fetchApi(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }
    postCreate = () => {
        var raw = JSON.stringify({
            "userId": 9,
            "id": 7756,
            "title": "test",
            "body": "test"
          });
        var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
            body: raw,
            
        };

        var url = "https://jsonplaceholder.typicode.com/posts/"
        this.fetchApi(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }
    putUpdate = () => {
        var raw = JSON.stringify({
            "userId": 1,
            "id": 7756,
            "title": "test",
            "body": "test"
          });
        var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            redirect: 'follow',
            body: raw,
            
        };

        var url = "https://jsonplaceholder.typicode.com/posts/1"
        this.fetchApi(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }
    delete = () => {
        var requestOptions = {
            method: 'DELETE',
            redirect: 'follow',            
        };
        var url = "https://jsonplaceholder.typicode.com/posts/1"

        this.fetchApi(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    render() {
        return (
            <>
            <h6 className="mt-2">FetchApi</h6>
            <div className="row">
                <div className="col-1"><button className="btn btn-primary" onClick={this.getById}>Get By Id</button></div>
                <div className="col-1"><button className="btn btn-primary" onClick={this.getAllId}>Get All</button></div>
                <div className="col-1"><button className="btn btn-primary" onClick={this.postCreate}>Post Create</button></div>
                <div className="col-1"><button className="btn btn-primary" onClick={this.putUpdate}>Put Update</button></div>
                <div className="col-1"><button className="btn btn-primary" onClick={this.delete}>Detele</button></div>
            </div>
            </>
            
        )
    }
}
export default FetchApi;