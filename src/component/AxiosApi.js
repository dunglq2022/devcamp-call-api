import { Component } from "react";
import axios from 'axios'

class AxiosApi extends Component {

    axiosLibraryCallApi = async (config) => {
        let res = await axios(config);

        return res.data
    }

    getById = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: { }
          };

        this.axiosLibraryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
        })
    }
    getAllId = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/',
            headers: { }
          };

        this.axiosLibraryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
        })
    }
    postCreate = () => {
        var data = JSON.stringify({
            "userId": 1,
            "id": 7756,
            "title": "test",
            "body": "test"
          });

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/',
            headers: { 
                'Content-Type': 'application/json'
              },
            data: data
          };

        this.axiosLibraryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
        })
    }
    putUpdate = () => {
        var data = JSON.stringify({
            "userId": 1,
            "id": 7756,
            "title": "test update",
            "body": "test update"
          });

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: { 
                'Content-Type': 'application/json'
              },
            data: data
          };

        this.axiosLibraryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
        })
    }
    delete = () => {
        let config = {
            method: 'delete',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/1'
          };

        this.axiosLibraryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
        })
    }

    render() {
        return (
            <>
            <h6 className="mt-4">AxiosApi</h6>
            <div className="row mt-1">
                <div className="col-1"><button className="btn btn-primary" onClick={this.getById}>Get By Id v</button></div>
                <div className="col-1"><button className="btn btn-primary" onClick={this.getAllId}>Get All</button></div>
                <div className="col-1"><button className="btn btn-primary" onClick={this.postCreate}>Post Create</button></div>
                <div className="col-1"><button className="btn btn-primary" onClick={this.putUpdate}>Put Update</button></div>
                <div className="col-1"><button className="btn btn-primary" onClick={this.delete}>Detele</button></div>
            </div>
            </>
            
        )
    }
}
export default AxiosApi;