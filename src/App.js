import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import AxiosApi from './component/AxiosApi';
import FetchApi from './component/FetchApi';

function App() {
  return (
    <div>
      <FetchApi/>
      <AxiosApi/>
    </div>
  );
}

export default App;
